//
//  main.m
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OpenGLCommon.h"

int main(int argc, char *argv[]) {

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    /*
    NSMutableString *result = [NSMutableString string];

    static const float zbottom = -0.5;
    static const Vertex3D vertices[]= {
            {-0.5, 0.5, -zbottom},
            {0.5, 0.5, -zbottom},
            {0.5, -0.5, -zbottom},
            {-0.5, -0.5, -zbottom},

            {-0.5, 0.5, zbottom},
            {0.5, 0.5, zbottom},
            {0.5, -0.5, zbottom},
            {-0.5, -0.5, zbottom},
    };

    static const GLubyte icosahedronFaces[] = {
            2,0,1,
            0,2,3,

            0,4,5,
            0,5,1,

            4,0,3,
            4,3,7,

            1,5,6,
            2,1,6,

            3,7,6,
            3,6,2,

            5,6,7,
            5,7,4,
    };

    int numOfFaces = sizeof(icosahedronFaces) / sizeof(GLubyte) / 3;
    int numOfVertices = sizeof(vertices) / sizeof(Vertex3D);


    Vector3D *surfaceNormals = calloc(numOfFaces, sizeof(Vector3D));

    // Calculate the surface normal for each triangle

    for (int i = 0; i < numOfFaces; i++)
    {
        Vertex3D vertex1 = vertices[icosahedronFaces[(i*3)]];
        Vertex3D vertex2 = vertices[icosahedronFaces[(i*3)+1]];
        Vertex3D vertex3 = vertices[icosahedronFaces[(i*3)+2]];
        Triangle3D triangle = Triangle3DMake(vertex1, vertex2, vertex3);
        Vector3D surfaceNormal = Triangle3DCalculateSurfaceNormal(triangle);
        Vector3DNormalize(&surfaceNormal);
        surfaceNormals[i] = surfaceNormal;
    }

    Vertex3D *normals = calloc(numOfVertices, sizeof(Vertex3D));
    [result appendString:@"static const Vector3D normals[] = {\n"];
    for (int i = 0; i < numOfVertices; i++)
    {
        int faceCount = 0;
        for (int j = 0; j < numOfFaces; j++)
        {
            BOOL contains = NO;
            for (int k = 0; k < 3; k++)
            {
                if (icosahedronFaces[(j * 3) + k] == i)
                    contains = YES;
            }
            if (contains)
            {
                faceCount++;
                normals[i] = Vector3DAdd(normals[i], surfaceNormals[j]);
            }
        }

        normals[i].x /= (GLfloat)faceCount;
        normals[i].y /= (GLfloat)faceCount;
        normals[i].z /= (GLfloat)faceCount;
        [result appendFormat:@"\t{%f, %f, %f},\n", normals[i].x, normals[i].y, normals[i].z];
    }
    [result appendString:@"};\n"];
    NSLog(result);
                  */

    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}

