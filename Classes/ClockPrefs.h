//
//  ClockPrefs.h
//  OpenGLESP2
//
//  Created by Dymov Eugene on 18.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "OpenGLCommon.h"

#ifndef OpenGLESP2_ClockPrefs_h
#define OpenGLESP2_ClockPrefs_h

#define CLOCK_BOTTOM_Z_COORDINATE   -0.3f
#define CLOCK_BASE_Z_COORDINATE     -0.15f
#define CLOCK_BASE_UP               0.04f
#define CLOCK_BOTTOM_SHIFT          0.01f
#define TEXTURE_SIZE                1024.0f

const int n = 32;
int ring_vertices = ((n + 1) * 2);
int circle_vertices = n + 2;

const Vector3D clockBaseTopCoords[] = {
        {-0.66, 0.24 + CLOCK_BASE_UP, 0.0},//1
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//2
        {-0.24, 0.64 + CLOCK_BASE_UP, 0.0},//3

        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//2
        {-0.24, 0.64 + CLOCK_BASE_UP, 0.0},//3
        {0.0  , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//4

        {-0.24, 0.64 + CLOCK_BASE_UP, 0.0},//3
        {0.0  , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//4
        {0.24 , 0.64 + CLOCK_BASE_UP, 0.0},//5

        {0.0  , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//4
        {0.24 , 0.64 + CLOCK_BASE_UP, 0.0},//5
        {0.5  ,  0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//6

        {0.24 , 0.64 + CLOCK_BASE_UP, 0.0},//5
        {0.5  ,  0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//6
        {0.66, 0.24 + CLOCK_BASE_UP,  0.0},//7
};

const Vector3D clockBaseBottomCoords[] = {
        {-0.66, -0.24 - CLOCK_BASE_UP, 0.0},//1
        {-0.5 , -0.8 - CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//2
        {-0.24, -0.64 - CLOCK_BASE_UP, 0.0},//3

        {-0.5 , -0.8 - CLOCK_BASE_UP , CLOCK_BASE_Z_COORDINATE},//2
        {-0.24, -0.64 - CLOCK_BASE_UP, 0.0},//3
        {0.0  , -0.8 - CLOCK_BASE_UP , CLOCK_BASE_Z_COORDINATE},//4

        {-0.24, -0.64 - CLOCK_BASE_UP, 0.0},//3
        {0.0  , -0.8 - CLOCK_BASE_UP , CLOCK_BASE_Z_COORDINATE},//4
        {0.24 , -0.64 - CLOCK_BASE_UP, 0.0},//5

        {0.0  , -0.8 - CLOCK_BASE_UP , CLOCK_BASE_Z_COORDINATE},//4
        {0.24 , -0.64 - CLOCK_BASE_UP, 0.0},//5
        {0.5  , -0.8 - CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//6

        {0.24 , -0.64 - CLOCK_BASE_UP, 0.0},//5
        {0.5  , -0.8 - CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},//6
        {0.66 , -0.24 - CLOCK_BASE_UP,  0.0},//7
};


const GLfloat clockBaseTopTextureCoords[] = {
        0.02,	0.92, 0.0,
        0.04,	0.99, 0.0,
        0.08,	0.97, 0.0,

        0.04,	0.99, 0.0,
        0.08,	0.97, 0.0,
        0.13,	1.00, 0.0,

        0.08,	0.97, 0.0,
        0.13,	1.00, 0.0,
        0.17,	0.97, 0.0,

        0.13,	1.00, 0.0,
        0.17,	0.97, 0.0,
        0.21,	0.99, 0.0,

        0.17,	0.97, 0.0,
        0.21,	0.99, 0.0,
        0.23,	0.92, 0.0,
};

const Vector3D clockLeftSide[] = {
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {-0.66, 0.24 + CLOCK_BASE_UP, -0.05},
        {-0.66, 0.24 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {-0.72, 0.0, -0.05},
        {-0.72, 0.0, CLOCK_BOTTOM_Z_COORDINATE},

        {-0.66, -1.0 * (0.24 + CLOCK_BASE_UP), -0.05},
        {-0.66, -1.0 * (0.24 + CLOCK_BASE_UP), CLOCK_BOTTOM_Z_COORDINATE},

        {-0.5 , -1.0 * (0.8 + CLOCK_BASE_UP), CLOCK_BASE_Z_COORDINATE},
        {-0.5 , -1.0 * (0.8 + CLOCK_BASE_UP), CLOCK_BOTTOM_Z_COORDINATE},

};

const Vector3D wristletFastenerTopCubeSide[] = {
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},

        {0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},

};

const Vector3D wristletFastenerBottomCubeSide[] = {
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.54 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.54 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.54 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.54 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
};

const GLfloat wristletFastenerBottomCubeSideTexture[] = {
        0.178,	0.508,
        0.194,	0.508,
        0.178,	0.491,

        0.194,	0.508,
        0.178,	0.491,
        0.194,	0.491,

        0.178,	0.508,
        0.194,	0.508,
        0.178,	0.491,

        0.194,	0.508,
        0.178,	0.491,
        0.194,	0.491,
};


const GLfloat wristletFastenerTopCubeSideTexture[] = {
        0.452,	0.965,
        0.472,	0.965,
        0.452,	0.945,

        0.472,	0.965,
        0.452,	0.945,
        0.472,	0.945,

        0.452,	0.965,
        0.472,	0.965,
        0.452,	0.945,

        0.472,	0.965,
        0.452,	0.945,
        0.472,	0.945,
};

const Vector3D wristletFastenerInnerCubeSide[] = {
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {-0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {-0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {-0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},

        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},

        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
};

const GLfloat wristletFastenerInnerCubeSideTexture[] = {
        0.568,	0.988,
        0.616,	0.988,
        0.568,	0.940,

        0.616,	0.988,
        0.568,	0.940,
        0.616,	0.940,

        0.568,	0.988,
        0.616,	0.988,
        0.568,	0.940,

        0.616,	0.988,
        0.568,	0.940,
        0.616,	0.940,
};

const Vector3D wristletFastener[] = {
        {-0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {0.35 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},

        {-0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},

        {-0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
        {0.35 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE},
};

const GLfloat wristetFastenerTexture[] = {
        0.327,	0.986,
        0.450,	0.986,

        0.327,	0.965,
        0.450,	0.965,

        0.327,	0.943,
        0.450,	0.943,
};

const Vector3D clockBottom[] = {
        {0.54 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE + CLOCK_BOTTOM_SHIFT},
        {-0.54 , 0.65 + CLOCK_BASE_UP, CLOCK_BOTTOM_Z_COORDINATE + CLOCK_BOTTOM_SHIFT},

        {0.72, 0.0, CLOCK_BOTTOM_Z_COORDINATE + CLOCK_BOTTOM_SHIFT},
        {-0.72, 0.0, CLOCK_BOTTOM_Z_COORDINATE + CLOCK_BOTTOM_SHIFT},

        {0.54 , -1.0 * (0.65 + CLOCK_BASE_UP), CLOCK_BOTTOM_Z_COORDINATE + CLOCK_BOTTOM_SHIFT},
        {-0.54 , -1.0 * (0.65 + CLOCK_BASE_UP), CLOCK_BOTTOM_Z_COORDINATE + CLOCK_BOTTOM_SHIFT},
};

const GLfloat clockBottomTexture[] = {
        0.051,	0.490,
        0.192,	0.490,

        0.014,	0.378,
        0.233,	0.378,

        0.053,	0.261,
        0.193,	0.261,
};

const Vector3D sideFiller[] = {
        {-0.5 , 0.8 + CLOCK_BASE_UP, CLOCK_BASE_Z_COORDINATE},
        {-0.66, 0.20 + CLOCK_BASE_UP, 0.0},
        {-0.66, 0.24 + CLOCK_BASE_UP, -0.05},

        {-0.66, -1.0 * (0.24 + CLOCK_BASE_UP), -0.05},
        {-0.66, -1.0 * (0.24 + CLOCK_BASE_UP), 0.0},
        {-0.5 , -1.0 * (0.8 + CLOCK_BASE_UP), CLOCK_BASE_Z_COORDINATE},
};

const GLfloat clockSideTextureCoords[] = {
        0.009,	0.578,
        0.009,	0.565,

        0.073,	0.607,
        0.073,	0.565,

        0.123,	0.609,
        0.123,	0.565,

        0.174,	0.607,
        0.174,	0.565,

        0.239,	0.578,
        0.239,	0.565,
};


#endif
