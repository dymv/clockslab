//
//  GLViewController.h
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLView.h"
#import "OpenGLCommon.h"

@interface GLViewController : UIViewController <GLViewDelegate>
{
    float angle;
    float scale, oldScale;
    GLuint texture[1];
    CGPoint rotationalAngle, oldAngle, angleOnEnd;
    NSDateFormatter *dateFormatter;

    Vector3D *clockDial, *clockDialNormals, *circleTexture, *bottomCircle, *bottomCircleNormals, *bottomCircleTexture, *arrowCircle, *arrowCircleTexture;
    Vector3D *aRing, *aRingNormals, *bRing, *bRingNormals, *ringTexture, *dialRing, *dialRingNormals, *dialRingTexture, *dialDarkRing, *dialDarkRingNormals;
    Vertex3D *sideFillerNormals, *wristletFastenerNormals, *wristletFastenerBottomCubeSideNormals, *wristletFastenerTopCubeSideNormals, *wristletFastenerInnerCubeSideNormals, *clockBottomNormals, *clockLeftSideNormals, *clockBaseTopNormals, *clockBaseBottomNormals;

}
@end
