//
//  GLViewController.m
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "GLViewController.h"
#import "ConstantsAndMacros.h"
#import "OpenGLCommon.h"
#import "ClockPrefs.h"
#import "Texture2D.h"

@interface GLViewController ()
- (Vector3D *)makeRingWithXPos:(float)x yPos:(float)y radius1:(float)radius1 radius2:(float)radius2 z1:(float)z1 z2:(float)z2;

- (Vector3D *)makeCircleWithRadius:(float)radius xPos:(float)x yPos:(float)y zPos:(float)z;

- (void)drawText:(NSString *)theString AtX:(float)X Y:(float)Y fontSize:(float)fontSize;

- (void)zooming:(UIPinchGestureRecognizer *)pinchGest;

- (void)createObjectsAndCalcNormals;

- (void)switchToOrtho;

- (void)switchBackToFrustum;


@end




@implementation GLViewController

- (Vector3D *)makeRingWithXPos:(float)x yPos:(float)y radius1:(float)radius1 radius2:(float)radius2 z1:(float)z1 z2:(float)z2 {

    Vector3D *ring = malloc(sizeof(Vector3D) * ring_vertices);

    float theta = 2 * M_PI / n;
    float ang = 0;
    int iter = 0;

    for (int i = 0; i < n; i++) {
        ring[iter].x = x + cosf(ang) * radius1;
        ring[iter].y = y + sinf(ang) * radius1;
        ring[iter].z = z1;

        iter++;

        ring[iter].x = x + cosf(ang) * (radius1 + radius2);
        ring[iter].y = y + sinf(ang) * (radius1 + radius2);
        ring[iter].z = z2;

        ang += theta;
        iter++;
    }

    float endAngle = 0.0f;

    ring[iter].x = x + cosf(endAngle) * radius1;
    ring[iter].y = y + sinf(endAngle) * radius1;
    ring[iter].z = z1;

    iter++;

    ring[iter].x = x + cosf(endAngle) * (radius1 + radius2);
    ring[iter].y = y + sinf(endAngle) * (radius1 + radius2);
    ring[iter].z = z2;



    return ring;
}

- (Vector3D *)makeCircleWithRadius:(float)radius xPos:(float)x yPos:(float)y zPos:(float)z {

    Vector3D *circle = malloc(sizeof(Vector3D) * circle_vertices);

    float theta = 2 * M_PI / n;
    float ang = 0;


    int iter = 0;
    circle[iter].x = x;
    circle[iter].y = y;
    circle[iter].z = z;
    iter++;

    for (int i = 0; i < n + 1; i++) {
        circle[iter].x = x + cos(ang) * radius;
        circle[iter].y = y + sin(ang) * radius;
        circle[iter].z = z;

        ang += theta;
        iter++;

    }

    return circle;
}

- (void)drawView:(GLView*)view;
{

    static GLfloat rot = 0.0;

    glLoadIdentity();
    glTranslatef(0.0, 0.0, scale);
    glRotatef(rotationalAngle.y, 1, 0, 0);
    glRotatef(rotationalAngle.x, 0, 1, 0);
    glClearColor(0.4, 0.4, 0.4, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnableClientState(GL_VERTEX_ARRAY);

    glDisable(GL_TEXTURE_2D);

    glColor4f(0.2, 0.2, 0.2, 1.0);

    glEnableClientState(GL_NORMAL_ARRAY);
    {//Dial Dark Ring Drawing


        glVertexPointer(3, GL_FLOAT, 0, dialDarkRing);
        glNormalPointer(GL_FLOAT, 0, dialDarkRingNormals);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, ring_vertices);

    }

    float color=0.6;
    glColor4f(color, color, color, 1.0);

    glVertexPointer(3, GL_FLOAT, 0, sideFiller);
    glNormalPointer(GL_FLOAT, 0, sideFillerNormals);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glRotatef(180.0, 0, 0, 1);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glRotatef(-180.0, 0, 0, 1);


    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glColor4f(1.0, 1.0, 1.0, 1.0);

// WRISTLET FASTENER

    glVertexPointer(3, GL_FLOAT, 0, wristletFastener);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristetFastenerTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, sizeof(wristletFastener) / sizeof(Vertex3D));
    glRotatef(180, 0, 0, 1);
    glVertexPointer(3, GL_FLOAT, 0, wristletFastener);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristetFastenerTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, sizeof(wristletFastener) / sizeof(Vertex3D));
    glRotatef(-180, 0, 0, 1);
// END WRISTLET FASTENER

// WRISTLET FASTENER BOTTOM CUBE

    glVertexPointer(3, GL_FLOAT, 0, wristletFastenerBottomCubeSide);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerBottomCubeSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristletFastenerBottomCubeSideTexture);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(wristletFastenerBottomCubeSide) / sizeof(Vertex3D));

    glRotatef(180, 0, 0, 1);
    glVertexPointer(3, GL_FLOAT, 0, wristletFastenerBottomCubeSide);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerBottomCubeSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristletFastenerBottomCubeSideTexture);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(wristletFastenerBottomCubeSide) / sizeof(Vertex3D));
    glRotatef(-180, 0, 0, 1);
// END WRISTLET FASTENER BOTTOM CUBE

//WRISTLET FASTENER TOP CUBE

    glVertexPointer(3, GL_FLOAT, 0, wristletFastenerTopCubeSide);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerTopCubeSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristletFastenerTopCubeSideTexture);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(wristletFastenerTopCubeSide) / sizeof(Vertex3D));

    glRotatef(180, 0, 0, 1);
    glVertexPointer(3, GL_FLOAT, 0, wristletFastenerTopCubeSide);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerTopCubeSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristletFastenerTopCubeSideTexture);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(wristletFastenerTopCubeSide) / sizeof(Vertex3D));
    glRotatef(-180, 0, 0, 1);
//END WRISTLET FASTENER TOP CUBE

//WRISTLET FASTENER INNER CUBE

    glVertexPointer(3, GL_FLOAT, 0, wristletFastenerInnerCubeSide);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerInnerCubeSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristletFastenerInnerCubeSideTexture);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(wristletFastenerInnerCubeSide) / sizeof(Vertex3D));

    glRotatef(180, 0, 0, 1);
    glVertexPointer(3, GL_FLOAT, 0, wristletFastenerInnerCubeSide);
    glNormalPointer(GL_FLOAT, 0, wristletFastenerInnerCubeSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, wristletFastenerInnerCubeSideTexture);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(wristletFastenerInnerCubeSide) / sizeof(Vertex3D));
    glRotatef(-180, 0, 0, 1);
//END WRISTLET FASTENER INNER CUBE

// CLOCK BOTTOM

    glVertexPointer(3, GL_FLOAT, 0, clockBottom);
    glNormalPointer(GL_FLOAT, 0, clockBottomNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, clockBottomTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 6);
// END CLOCK BOTTOM

//CLOCK LEFT SIDE

    glVertexPointer(3, GL_FLOAT, 0, clockLeftSide);
    glNormalPointer(GL_FLOAT, 0, clockLeftSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, clockSideTextureCoords);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 10);

    glRotatef(180, 0, 0, 1);
    glVertexPointer(3, GL_FLOAT, 0, clockLeftSide);
    glNormalPointer(GL_FLOAT, 0, clockLeftSideNormals);
    glTexCoordPointer(2, GL_FLOAT, 0, clockSideTextureCoords);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 10);
    glRotatef(-180, 0, 0, 1);
//END CLOCK LEFT SIDE

// CLOCK BASE DRAWING
    //TOP
    glVertexPointer(3, GL_FLOAT, 0, clockBaseTopCoords);
    glNormalPointer(GL_FLOAT, 0, clockBaseTopNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, clockBaseTopTextureCoords);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 15);
    // END TOP

    // BOTTOM
    glVertexPointer(3, GL_FLOAT, 0, clockBaseBottomCoords);
    glNormalPointer(GL_FLOAT, 0, clockBaseBottomNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, clockBaseTopTextureCoords);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 15);
    // END BOTTOM
//END CLOCK BASE DARAWING

//CLOCK DIAL DRAWING
    glVertexPointer(3, GL_FLOAT, 0, clockDial);
    glNormalPointer(GL_FLOAT, 0, clockDialNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, circleTexture);
    glDrawArrays(GL_TRIANGLE_FAN, 0, circle_vertices);
// END CLOCK DIAL DRAWING

//DIAL RING
    glVertexPointer(3, GL_FLOAT, 0, dialRing);
    glNormalPointer(GL_FLOAT, 0, dialRingNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, dialRingTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, ring_vertices);
// END DIAL RING

//A RING DRAWING
    glVertexPointer(3, GL_FLOAT, 0, aRing);
    glNormalPointer(GL_FLOAT, 0, aRingNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, ringTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, ring_vertices);
//END A RING DRAWING

//B RING DRAWING
    glVertexPointer(3, GL_FLOAT, 0, bRing);
    glNormalPointer(GL_FLOAT, 0, bRingNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, ringTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, ring_vertices);
//END B RING DRAWING

//BOTTOM CIRCLE
    glRotatef(180.0, 0, 1, 0);
    glVertexPointer(3, GL_FLOAT, 0, bottomCircle);
    glNormalPointer(GL_FLOAT, 0, bottomCircleNormals);
    glTexCoordPointer(3, GL_FLOAT, 0, bottomCircleTexture);
    glDrawArrays(GL_TRIANGLE_FAN, 0, circle_vertices);
    glRotatef(-180.0, 0, 1, 0);
//END BOTTOM CIRCLE






    glDisableClientState(GL_NORMAL_ARRAY);

    NSDate *date = [NSDate date];
    NSString *timeString = [[NSString alloc] initWithFormat:@"%@", [dateFormatter stringFromDate:date]];




    glDisable(GL_LIGHTING);

    int h = [[timeString substringWithRange:NSMakeRange(0, 2)] intValue];
    int m = [[timeString substringWithRange:NSMakeRange(3, 2)] intValue];
    int s = [[timeString substringWithRange:NSMakeRange(5, 2)] intValue];

    const float hourArrowZCoord = -0.05;
    const Vertex3D hourArrow[] = {
            {0.0, 0.26, hourArrowZCoord},
            {0.027,0.0, hourArrowZCoord},
            {-0.027,0.0, hourArrowZCoord},

            {0.027,0.0, hourArrowZCoord},
            {-0.027,0.0, hourArrowZCoord},
            {0.0,-0.05,hourArrowZCoord},

    };

    const GLfloat hourArrowTexture[] = {
            0.565,	0.484,
            0.360,	0.465,
            0.360,	0.508,

            0.360,	0.465,
            0.360,	0.508,
            0.302,	0.487,
    };

    const Vertex3D minArrow[] = {
            {0.0, 0.40, hourArrowZCoord},
            {0.022,0.0, hourArrowZCoord},
            {-0.022,0.0, hourArrowZCoord},

            {0.022,0.0, hourArrowZCoord},
            {-0.022,0.0, hourArrowZCoord},
            {0.0,-0.1,hourArrowZCoord},
    };

    glPushMatrix();
    glRotatef(-(360.0 / 12.0 * (h+m/60.0)), 0, 0, 1.0);
    glColor4f(1.0, 1.0, 1.0, 1.0);
    glVertexPointer(3, GL_FLOAT, 0, hourArrow);
    glTexCoordPointer(2, GL_FLOAT, 0, hourArrowTexture);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glPopMatrix();
    glPushMatrix();

    glTranslatef(0.0, 0.0, 0.0025);
    glRotatef(-(360.0 / 60.0 * (m + s/60.0)), 0, 0, 1.0);
    glVertexPointer(3, GL_FLOAT, 0, minArrow);
    glTexCoordPointer(2, GL_FLOAT, 0, hourArrowTexture);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glPopMatrix();
    glVertexPointer(3, GL_FLOAT, 0, arrowCircle);
    glTexCoordPointer(3, GL_FLOAT, 0, arrowCircleTexture);
    glDrawArrays(GL_TRIANGLE_FAN, 0, circle_vertices);


    glDisable(GL_TEXTURE_2D);
     glDisableClientState(GL_TEXTURE_COORD_ARRAY);



    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);


    glTranslatef(0.0, 0.0, -0.05);
    [self drawText:timeString AtX:-0.3 Y:0.53 fontSize:24];

    glDisableClientState(GL_VERTEX_ARRAY);




    static NSTimeInterval lastDrawTime;
    if (lastDrawTime)
    {
        NSTimeInterval timeSinceLastDraw = [NSDate timeIntervalSinceReferenceDate] - lastDrawTime;
        rot+=  30 * timeSinceLastDraw;

    }
    lastDrawTime = [NSDate timeIntervalSinceReferenceDate];


    [timeString release];
}

- (void)drawText:(NSString *)theString AtX:(float)X Y:(float)Y fontSize:(float)fontSize {
// Use black
    glColor4f(1.0, 0, 0, 1.0);
    glClearColor(1.0, 0.0, 0.0, 1.0);

    NSString *time = [[NSString alloc] initWithFormat:@"%@", [theString substringWithRange:NSMakeRange(0, 5)]];
    NSString *sec = [[NSString alloc] initWithFormat:@"%@", [theString substringWithRange:NSMakeRange(5, 2)]];
    NSString *ampm = [[NSString alloc] initWithFormat:@"%@", [theString substringWithRange:NSMakeRange(7, 2)]];

    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 60)];
    container.backgroundColor = [UIColor clearColor];

    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.frame = CGRectMake(16, 36, 50, 20);
    timeLabel.textColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0];
    timeLabel.font = [UIFont fontWithName:@"DS-Digital" size:20];
    timeLabel.textAlignment = UITextAlignmentCenter;
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.text = time;

    [container addSubview:timeLabel];

    UILabel *secLabel = [[UILabel alloc] init];

    secLabel.textColor = timeLabel.textColor;
    secLabel.textAlignment = timeLabel.textAlignment;
    secLabel.backgroundColor = timeLabel.backgroundColor;
    secLabel.frame = CGRectMake(24, 28, 30, 10);
    secLabel.text = sec;
    secLabel.font = [UIFont fontWithName:@"DS-Digital" size:14];

    [container addSubview:secLabel];

    UILabel *ampmLabel = [[UILabel alloc] init];
    ampmLabel.textColor = timeLabel.textColor;
    ampmLabel.textAlignment = timeLabel.textAlignment;
    ampmLabel.backgroundColor = timeLabel.backgroundColor;
    ampmLabel.frame = CGRectMake(0, 33, 17, 10);
    ampmLabel.text = ampm;
    ampmLabel.font = [UIFont fontWithName:@"DS-Digital" size:14];

    [container addSubview:ampmLabel];


    UIGraphicsBeginImageContext(container.frame.size);
    [container.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *layerImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

// Set up texture
//    Texture2D* statusTexture = [[Texture2D alloc] initWithString:theString dimensions:CGSizeMake(150, 150) alignment:UITextAlignmentLeft fontName:@"DS-Digital" fontSize:24];
    Texture2D *statusTexture = [[Texture2D alloc] initWithCGImage:layerImage.CGImage orientation:UIImageOrientationUp sizeToFit:NO pixelFormat:kTexture2DPixelFormat_RGBA4444];

// Bind texture
    glBindTexture(GL_TEXTURE_2D, [statusTexture name]);

// Enable modes needed for drawing
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
//    glBlendFunc(GL_ONE, GL_ONE);
//    glBlendFunc(GL_SRC_COLOR, GL_ONE);

// Draw
    [statusTexture drawInRect:CGRectMake(X,Y-1, 1.0, 0.5)];


// Disable modes so they don't interfere with other parts of the program
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);

    [statusTexture release];
    [timeLabel release];
    [ampm release];
    [sec release];
    [time release];
    [container release];
    [secLabel release];
    [ampmLabel release];
}

- (void)zooming:(UIPinchGestureRecognizer *)pinchGest {
    switch (pinchGest.state) {
        case UIGestureRecognizerStateBegan:
        {

            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            scale = oldScale/pinchGest.scale;
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            oldScale = scale;
            break;
        }
    }
}

- (void)createObjectsAndCalcNormals {
    float texCircCenX = 0.123;
    float texCircCenY = 1.0 - 0.22;
    float rad = 80.0 / TEXTURE_SIZE;

    arrowCircle = [self makeCircleWithRadius:0.025 xPos:0.0 yPos:0.0 zPos:-0.045];
    arrowCircleTexture = [self makeCircleWithRadius:58.0/ TEXTURE_SIZE xPos:400/ TEXTURE_SIZE yPos:1.0 - 649/ TEXTURE_SIZE zPos:0.0];

    circleTexture = [self makeCircleWithRadius:rad xPos:texCircCenX yPos:texCircCenY zPos:0.0];
    //
    clockDial = [self makeCircleWithRadius:0.467 xPos:0.0 yPos:0.0 zPos:-0.06];

    clockDialNormals = malloc(sizeof(Vector3D) * circle_vertices);

    for (int i=0; i<circle_vertices; i++) {
        clockDialNormals[i].x = clockDial[i].x * 2;
        clockDialNormals[i].y = clockDial[i].y * 2;
        clockDialNormals[i].z = 1.85;
    }
    //

    dialRing = [self makeRingWithXPos:0.0 yPos:0.0 radius1:0.467 radius2:0.233 z1:0.01 z2:0.01];
    dialRingNormals = malloc(sizeof(Vector3D) * ring_vertices);
    for (int i=0; i<ring_vertices; i++) {
        dialRingNormals[i].x = dialRing[i].x * 2;
        dialRingNormals[i].y = dialRing[i].y * 2;
        dialRingNormals[i].z = 1.85;
    }

    //
    dialDarkRing = [self makeRingWithXPos:0.0 yPos:0.0 radius1:0.467 radius2:0.0 z1:0.01 z2:-0.06];
    dialDarkRingNormals = malloc(sizeof(Vector3D) * ring_vertices);

    for (int i=0; i<ring_vertices; i++) {
        dialDarkRingNormals[i].x = dialDarkRing[i].x / 2;
        dialDarkRingNormals[i].y = dialDarkRing[i].y / 2 ;
        dialDarkRingNormals[i].z = dialDarkRing[i].z * 0;
    }
    //

    dialRingTexture = [self makeRingWithXPos:texCircCenX yPos:texCircCenY radius1:81.0/ TEXTURE_SIZE radius2:40.0/TEXTURE_SIZE z1:0.0 z2:0.0];


    bottomCircle = [self makeCircleWithRadius:0.61 xPos:0.0 yPos:0.0 zPos:-CLOCK_BOTTOM_Z_COORDINATE];
    bottomCircleNormals = malloc(sizeof(Vector3D) * circle_vertices);

    for (int i=0; i<circle_vertices; i++) {
        bottomCircleNormals[i].x = bottomCircle[i].x * 2;
        bottomCircleNormals[i].y = bottomCircle[i].y * 2;
        bottomCircleNormals[i].z = bottomCircle[i].z * 4;
    }

    bottomCircleTexture = [self makeCircleWithRadius:95.0/TEXTURE_SIZE xPos:127.0/TEXTURE_SIZE yPos:1.0 - 634.0/TEXTURE_SIZE zPos:0.0];

    //
    aRing = [self makeRingWithXPos:0.0 yPos:0.0 radius1:0.65 radius2:0.08 z1:0.0 z2:0.0];

    aRingNormals = malloc(sizeof(Vector3D) * ring_vertices);
    for (int i=0; i < ring_vertices; i++) {
        aRingNormals[i].x = aRing[i].x * 2;
        aRingNormals[i].y = aRing[i].y * 2;
        aRingNormals[i].z = 0.5;

    }
    //

    bRing = [self makeRingWithXPos:0.0 yPos:0.0 radius1:0.73 radius2:0.0 z1:0.01 z2:-0.05];

    bRingNormals = malloc(sizeof(Vector3D) * ring_vertices);
    for (int i=0; i<ring_vertices; i++) {
        bRingNormals[i].x = bRing[i].x * 2;
        bRingNormals[i].y = bRing[i].y;
        bRingNormals[i].z = bRing[i].z;

    }

    float texRingCenX = 403.0 / TEXTURE_SIZE;
    float texRingCenY = 1.0 - 224.0 / TEXTURE_SIZE;
    float ringRad1 = 122.0 / TEXTURE_SIZE;
    float ringRad2 = (131.0 - 122.0) / TEXTURE_SIZE;

    ringTexture = [self makeRingWithXPos:texRingCenX yPos:texRingCenY radius1:ringRad1 radius2:ringRad2 z1:0.0 z2:0.0];

    unsigned long sideFillerLength = sizeof(sideFiller) / sizeof(Vector3D);
    sideFillerNormals = calloc(sideFillerLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(sideFillerLength); i++) {
        sideFillerNormals[i].x = sideFiller[i].x * 2;
        sideFillerNormals[i].y = sideFiller[i].y * 2;
        sideFillerNormals[i].z = sideFiller[i].z * 2;
    }                            \

    unsigned long wristletFastenerLength = sizeof(wristletFastener) / sizeof(Vector3D);
    wristletFastenerNormals = calloc(wristletFastenerLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(wristletFastenerLength); i++) {
        wristletFastenerNormals[i].x = wristletFastener[i].x * 2;
        wristletFastenerNormals[i].y = wristletFastener[i].y * 2;
        wristletFastenerNormals[i].z = wristletFastener[i].z * 4;
    }


    unsigned long wristletFastenerBottomCubeSideLength = sizeof(wristletFastenerBottomCubeSide) / sizeof(Vector3D);
    wristletFastenerBottomCubeSideNormals = calloc(wristletFastenerBottomCubeSideLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(wristletFastenerBottomCubeSideLength); i++) {
        wristletFastenerBottomCubeSideNormals[i].x = wristletFastenerBottomCubeSide[i].x * 2;
        wristletFastenerBottomCubeSideNormals[i].y = wristletFastenerBottomCubeSide[i].y * 2;
        wristletFastenerBottomCubeSideNormals[i].z = wristletFastenerBottomCubeSide[i].z * 4;
    }

    unsigned long wristletFastenerTopCubeSideLength = sizeof(wristletFastenerTopCubeSide) / sizeof(Vector3D);
    wristletFastenerTopCubeSideNormals = calloc(wristletFastenerTopCubeSideLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(wristletFastenerTopCubeSideLength); i++) {
        wristletFastenerTopCubeSideNormals[i].x = wristletFastenerTopCubeSide[i].x ;
        wristletFastenerTopCubeSideNormals[i].y = wristletFastenerTopCubeSide[i].y * 2;
        wristletFastenerTopCubeSideNormals[i].z = wristletFastenerTopCubeSide[i].z ;
    }


    unsigned long wristletFastenerInnerCubeSideLength = sizeof(wristletFastenerInnerCubeSide) / sizeof(Vector3D);
    wristletFastenerInnerCubeSideNormals = calloc(wristletFastenerInnerCubeSideLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(wristletFastenerInnerCubeSideLength); i++) {
        wristletFastenerInnerCubeSideNormals[i].x = wristletFastenerInnerCubeSide[i].x * -2;
        wristletFastenerInnerCubeSideNormals[i].y = wristletFastenerInnerCubeSide[i].y * 2;
        wristletFastenerInnerCubeSideNormals[i].z = wristletFastenerInnerCubeSide[i].z * 2;
    }

    unsigned long clockBottomLength = sizeof(clockBottom) / sizeof(Vector3D);
    clockBottomNormals = calloc(clockBottomLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(clockBottomLength); i++) {
        clockBottomNormals[i].x = clockBottom[i].x * 1;
        clockBottomNormals[i].y = clockBottom[i].y * 1;
        clockBottomNormals[i].z = clockBottom[i].z * 4;
    }

    unsigned long clockLeftSideLength = sizeof(clockLeftSide) / sizeof(Vector3D);
    clockLeftSideNormals = calloc(clockLeftSideLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(clockLeftSideLength); i++) {
        clockLeftSideNormals[i].x = clockLeftSide[i].x * 2;
        clockLeftSideNormals[i].y = clockLeftSide[i].y * 2;
        clockLeftSideNormals[i].z = clockLeftSide[i].z * 2;
    }

    unsigned long clockBaseTopLength = sizeof(clockBaseTopCoords) / sizeof(Vector3D);
    clockBaseTopNormals = calloc(clockBaseTopLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(clockBaseTopLength); i++) {
        clockBaseTopNormals[i].x = clockBaseTopCoords[i].x * 2;
        clockBaseTopNormals[i].y = clockBaseTopCoords[i].y * 2;
        clockBaseTopNormals[i].z = clockBaseTopCoords[i].z + 1;
    }

    unsigned long clockBaseBottomLength = sizeof(clockBaseBottomCoords) / sizeof(Vector3D);
    clockBaseBottomNormals = calloc(clockBaseBottomLength, sizeof(Vertex3D));

    for (int i=0; i<(int)(clockBaseBottomLength); i++) {
        clockBaseBottomNormals[i].x = clockBaseBottomCoords[i].x * 2;
        clockBaseBottomNormals[i].y = clockBaseBottomCoords[i].y * 2;
        clockBaseBottomNormals[i].z = clockBaseBottomCoords[i].z + 1;
    }

}

-(void)switchToOrtho
{
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrthof(0, self.view.bounds.size.width, 0, self.view.bounds.size.height, -5, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

-(void)switchBackToFrustum
{
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

-(void)setupView:(GLView*)view
{
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mmssa"];


    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zooming:)];
    [self.view addGestureRecognizer:pinchGestureRecognizer];
    [pinchGestureRecognizer release];
    scale = -3.0;
    oldScale = -3.0;

    const GLfloat zNear = 0.01, zFar = 1000.0, fieldOfView = 45.0;
    GLfloat size;
    glMatrixMode(GL_PROJECTION);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    size = zNear * tanf(DEGREES_TO_RADIANS(fieldOfView) / 2.0);
    CGRect rect = view.bounds;
    glFrustumf(-size, size, -size / (rect.size.width / rect.size.height), size /
            (rect.size.width / rect.size.height), zNear, zFar);
//    glOrthof(-1.0,                                          // Left
//               1.0,                                          // Right
//              -1.0 / (rect.size.width / rect.size.height),   // Bottom
//               1.0 / (rect.size.width / rect.size.height),   // Top
//               0.01,                                         // Near
//               10000.0);

    glViewport(0, 0, rect.size.width, rect.size.height);
    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_TEXTURE_2D);
//    glEnable(GL_BLEND);
//    glBlendFunc(GL_ONE, GL_SRC_COLOR);

    glGenTextures(1, &texture[0]);
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    NSString *path = [[NSBundle mainBundle] pathForResource:@"full_texture" ofType:@"png"];
    NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
    UIImage *image = [[UIImage alloc] initWithData:texData];
    if (image == nil)
        NSLog(@"Do real error checking here");

    GLuint width = CGImageGetWidth(image.CGImage);
    GLuint height = CGImageGetHeight(image.CGImage);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *imageData = malloc( height * width * 4 );
    CGContextRef context = CGBitmapContextCreate( imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
    CGColorSpaceRelease( colorSpace );
    CGContextClearRect( context, CGRectMake( 0, 0, width, height ) );
    CGContextTranslateCTM( context, 0, height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), image.CGImage );

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

    CGContextRelease(context);

    free(imageData);
    [image release];
    [texData release];

    [self createObjectsAndCalcNormals];

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    const GLfloat light0Ambient[] = {0.25, 0.25, 0.25, 1.0};
    glLightfv(GL_LIGHT0, GL_AMBIENT, light0Ambient);

    const GLfloat light0Diffuse[] = {0.5, 0.5, 0.5, 1.0};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light0Diffuse);

    const GLfloat light0Specular[] = {0.7, 0.7, 0.7, 1.0};
    glLightfv(GL_LIGHT0, GL_SPECULAR, light0Specular);

    const GLfloat light0Position[] = {0.0, 0.0, 2.0, 0.0};
    glLightfv(GL_LIGHT0, GL_POSITION, light0Position);

    const GLfloat light0Direction[] = {0.0, 0.0, -1.0};
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0Direction);

//    const GLfloat light0Shineness[] = {0.9, 0.9, 0.9, 1.0};
//    glLightfv(GL_LIGHT0, GL_SHININESS, light0Shineness);

    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 20.0);


    glLoadIdentity();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}
- (void)dealloc
{
    free(clockDial);
    free(clockDialNormals);
    free(circleTexture);
    free(bottomCircle);
    free(bottomCircleNormals);
    free(bottomCircleTexture);
    free(ringTexture);
    free(dialRing);
    free(dialRingNormals);
    free(dialRingTexture);
    free(dialDarkRing);
    free(dialDarkRingNormals);
    free(aRing);
    free(aRingNormals);
    free(bRing);
    free(bRingNormals);
    free(sideFillerNormals);
    free(wristletFastenerNormals);
    free(wristletFastenerBottomCubeSideNormals);
    free(wristletFastenerTopCubeSideNormals);
    free(wristletFastenerInnerCubeSideNormals);
    free(clockBottomNormals);
    free(clockLeftSideNormals);
    free(clockBaseTopNormals);
    free(clockBaseBottomNormals);

    [dateFormatter release];
    [super dealloc];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event touchesForView:self.view] anyObject];
    oldAngle = [touch locationInView:self.view];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event touchesForView:self.view] anyObject];
    rotationalAngle = CGPointMake(
            [touch locationInView:self.view].x - oldAngle.x + angleOnEnd.x,
            [touch locationInView:self.view].y - oldAngle.y + angleOnEnd.y
    );
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    angleOnEnd = rotationalAngle;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}


@end
